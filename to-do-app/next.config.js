/** @type {import('next').NextConfig} */
const nextConfig = {
    env: {
        apiUrl: 'https://localhost:8003/',
    },
}

module.exports = nextConfig
