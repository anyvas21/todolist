'use client'

import { PieChart, Pie, Sector, Cell, ResponsiveContainer } from 'recharts';
import { useEffect, useState } from 'react';
import { Card, CardBody, CardFooter, Chip } from "@nextui-org/react";
import moment from 'moment';
import axios from 'axios';

export default function Home() {
  const [data, setData] = useState([]);
  const [date, setDate] = useState('');

  useEffect(() => {
    axios.get(`${process.env.apiUrl}api/v1/ToDoList/PieChartData`)
      .then((response) => {
        setData(response.data);
      })
      .catch(() => {
        setData([]);
      });

    setDate(moment().format("DD.MM.YYYY hh:mm:ss"));
  }, []);

  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  };

  return (
    <div className="gap-2 grid grid-cols-2 sm:grid-cols-4">
      <Card shadow="sm" key={`chart`}>
        <CardBody >
          <div className="flex text-small justify-center">
            <PieChart width={400} height={400}>
              <Pie
                data={data}
                cx="50%"
                cy="50%"
                labelLine={false}
                label={renderCustomizedLabel}
                outerRadius={180}
                fill="#8884d8"
                dataKey="value"
              >
                {data.map((entry, index) => (
                  <Cell key={`cell-${index}`} fill={entry.colore} />
                ))}
              </Pie>
            </PieChart>
          </div>
        </CardBody>
        <CardFooter className="flex flex-col">
          {data.map((entry, index) => (
             <p key={index} style={{background:`${entry.colore}`}} className="text-default-300 text-medium">{entry.name}</p>
          ))}
        </CardFooter>
      </Card>
    </div>
  )
}
