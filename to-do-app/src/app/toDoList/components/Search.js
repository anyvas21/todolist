import { Input } from "@nextui-org/react";

export default function App(props) {
    let { searchValue, setSearchValue } = props;
    return (
        <div className="w-full flex flex-col gap-2 max-w-[240px]">
            <Input
                label="Search"
                placeholder="Enter your search value here ... "
                value={searchValue}
                onValueChange={setSearchValue}
            />
        </div>
    );
}