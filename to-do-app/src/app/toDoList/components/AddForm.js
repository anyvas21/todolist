'use client'

import { Button, Input, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, Switch, useDisclosure } from "@nextui-org/react";
import axios from "axios";
import { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { FaPlus } from "react-icons/fa";

export default function App(props) {
  let { setItems } = props;

  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const [loading, setLoading] = useState(false);

  const { register, handleSubmit, control, formState: { errors } } = useForm({
    defaultValues: {
      task: '',
      isImportant: false,
    }
  });

  const onSubmit = data => {
    setLoading(true);

    axios.post(`${process.env.apiUrl}api/v1/ToDoList/CreateItem`, {
      "id": 0,
      "name": data.task,
      "date": new Date(),
      "isImportant": data.isImportant,
      "isDone": false
    }, {
      headers: { "Access-Control-Allow-Origin": "*" }
    })
      .then((response) => {
        setItems(response.data)
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });
  }

  return (
    <>
      <div className="flex flex-row">
        <Button
          onPress={onOpen}
          endContent={<FaPlus/>}
          variant="faded"
          color="primary"
          aria-label="add task"
        >
          Add Task
        </Button>
      </div>
      <Modal isOpen={isOpen} onOpenChange={onOpenChange}>
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="flex flex-col gap-1">Add task</ModalHeader>
              <ModalBody>
                <div className="flex flex-col w-full">
                  <form className="flex flex-col gap-4" onSubmit={handleSubmit(onSubmit)}>
                    <Input
                      isRequired
                      label="Task"
                      placeholder="Enter your task"
                      type="text"
                      {...register("task")}
                    />
                    <Controller
                      name="isImportant"
                      control={control}
                      render={({ field }) => <Switch {...field}>
                        Is this task important ?
                      </Switch>}
                    />

                    <div className="flex gap-2 justify-end">
                      <Button fullWidth color="primary" type="submit" isLoading={loading}>
                        Add
                      </Button>
                    </div>
                  </form>
                </div>
              </ModalBody>
              <ModalFooter>
                <Button color="danger" variant="light" onPress={onClose}>
                  Close
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
    </>
  );
}
