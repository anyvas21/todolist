import { Button, Card, CardBody } from "@nextui-org/react";
import React from "react";
import { FaRegStar, FaStar, FaThumbsDown, FaThumbsUp, FaTrash } from "react-icons/fa";
import moment from "moment/moment";
import axios from "axios";
import { motion } from 'framer-motion'

export default function App(props) {
  let { isDone: isDoneProp, isImportant: isImportantProp, name, date, id, setItems } = props;

  const [isImportant, setIsImportant] = React.useState(isImportantProp);
  const [isDone, setIsDone] = React.useState(isDoneProp);

  const deleteTask = () => {
    axios.delete(`${process.env.apiUrl}api/v1/ToDoList/Delete?id=${id}`, {
      headers: { "Access-Control-Allow-Origin": "*", 'Content-Type': 'application/json; charset=utf-8' }
    })
      .then((response) => {
        setItems(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const markTaskUpdate = () => {
    setIsDone(!isDone);

    axios.put(`${process.env.apiUrl}api/v1/ToDoList/UpdateDone`, id, {
      headers: { "Access-Control-Allow-Origin": "*", 'Content-Type': 'application/json; charset=utf-8' }
    })
      .then((response) => {
        setItems(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const importanceTaskUpdate = () => {
    setIsImportant(!isImportant)

    axios.put(`${process.env.apiUrl}api/v1/ToDoList/UpdateImportance`, id, {
      headers: { "Access-Control-Allow-Origin": "*", 'Content-Type': 'application/json; charset=utf-8' }
    })
      .then((response) => {
        setItems(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  return (
    <>
      <motion.div
        whileHover={{ scale: 1.02 }}
        whileTap={{
          scale: 0.95,
          borderRadius: "100%"
        }}
      >
        <Card key={`${id}${date}`} style={{ marginBottom: "4px" }}>
          <CardBody>
            <div className="flex items-center justify-between">
              <div>
                <p className="text-default-300 text-medium">{isDone ? <strike>{name}</strike> : <>{name}</>}</p>
                <p className="text-default-400 text-small">{moment(date).format("DD.MM.yyyy HH:mm:ss")}</p>
              </div>
              <div className="flex flex-row gap-1">
                <Button
                  isIconOnly
                  className={!isImportant ? "bg-transparent text-foreground border-default-200" : ""}
                  color="warning"
                  variant="faded"
                  onPress={() => importanceTaskUpdate(!isImportant)}
                >
                  {!isImportant ? <FaRegStar /> : <FaStar />}
                </Button>

                <Button
                  isIconOnly
                  className={!isDone ? "bg-transparent text-foreground border-default-200" : ""}
                  color={!isDone ? "priimary" : "secondary"}
                  variant="faded"
                  onPress={() => markTaskUpdate(!isDone)}
                >
                  {!isDone ? <FaThumbsDown /> : <FaThumbsUp />}
                </Button>

                <Button
                  isIconOnly
                  color="danger"
                  variant="faded"
                  onPress={() => deleteTask(id)}
                >
                  {< FaTrash />}
                </Button>

              </div>
            </div>
          </CardBody>
        </Card>
      </motion.div>
    </>
  );
}