import { Badge } from "@nextui-org/react";

export default function App(props) {
    let { counnt, title, IconTab } = props;

    return (
        <Badge content={counnt} color="primary">
            <div className="flex flex-row gap-2 min-w-100">
                <IconTab />
                <div className="flex mr-4">
                    <p >{title}</p>
                </div>
            </div>
        </Badge>
    );
}