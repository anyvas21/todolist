'use client'

import { Divider, Tab, Tabs, Progress } from "@nextui-org/react";
import axios from "axios";
import { useEffect, useState } from "react";
import { FaCheck, FaList, FaStar } from "react-icons/fa";
import AddForm from './components/AddForm';
import Item from './components/Item';
import Search from './components/Search';
import TabTitle from './components/TabTitle';

export default function App() {
  const [items, setItems] = useState([]);
  const [itemsDone, setItemsDone] = useState([]);
  const [itemsImportant, setItemsImportant] = useState([]);
  //
  const [itemsFiltered, setItemsFiltered] = useState([]);
  const [searchValue, setSearchValue] = useState([]);
  //
  const [itemsCount, setItemsCount] = useState(0);
  const [itemsDoneCount, setItemsDoneCount] = useState(0);
  const [itemsImportantCount, setItemsImportantCount] = useState(0);
  //
  const [loadingList, setLoadingList] = useState(false);

  const UpdateList = () => {
    setLoadingList(true);
    axios.get(`${process.env.apiUrl}api/v1/ToDoList/ItemList`)
      .then((response) => {
        setItems(response.data);
        setLoadingList(false);
      })
      .catch(() => {
        setItems([]);
        setLoadingList(false);
      });
  }

  useEffect(() => {
    UpdateList();
  }, [])

  useEffect(() => {
    const tempItemsFiltered = items.filter(item => item.name.includes(searchValue));
    //
    const tempItemsDone = tempItemsFiltered.filter((item) => item.isDone === true);
    const tempItemsImportant = tempItemsFiltered.filter((item) => item.isImportant === true);
    //
    setItemsFiltered(tempItemsFiltered);
    setItemsDone(tempItemsDone);
    setItemsImportant(tempItemsImportant);
    //
    setItemsCount(tempItemsFiltered.length);
    setItemsDoneCount(tempItemsDone.length);
    setItemsImportantCount(tempItemsImportant.length);
  }, [items, searchValue])

  return (
    <div className="flex justify-center">
      {!loadingList ?
        <div className="flex flex-col gap-2" style={{ width: "50%", marginTop: "18px" }}>
          <AddForm setItems={setItems} />
          <Search searchValue={searchValue} setSearchValue={setSearchValue} />
          <Divider />
          <Tabs aria-label="Options" >
            <Tab key="all"
              title={
                <TabTitle
                  counnt={itemsCount}
                  title="All ..."
                  IconTab={FaList}
                />
              }
            >
              {itemsFiltered.map((item, index) => (
                <Item {...item} key={index} setItems={setItems} />
              ))}
            </Tab>
            <Tab key="important"
              title={
                <TabTitle
                  counnt={itemsImportantCount}
                  title="Important ..."
                  IconTab={FaStar}
                />
              }
            >
              {itemsImportant.map((item, index) => (
                <Item {...item} key={index} />
              ))}
            </Tab>
            <Tab key="done"
              title={
                <TabTitle
                  counnt={itemsDoneCount}
                  title="Done ..."
                  IconTab={FaCheck}
                />
              }
            >
              {itemsDone.map((item, index) => (
                <Item {...item} key={index} />
              ))}
            </Tab>
          </Tabs>
        </div>
        :
        <Progress
          size="sm"
          isIndeterminate
          aria-label="Loading..."
          className="max-w-md"
        />
      }
    </div>
  );
}