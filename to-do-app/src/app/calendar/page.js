"use client"

import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import { useState, useEffect } from 'react';
import axios from 'axios';

export default function Calendar() {
  const [events, setEvents] = useState([]);

  useEffect(() => {
    axios.get(`${process.env.apiUrl}api/v1/ToDoList/CalendarData`)
      .then((response) => {
        setEvents(response.data);
      })
      .catch(() => {
        setEvents([]);
      });

  }, []);

  return (
    <div>
      <h1>Tasks</h1>
      <FullCalendar
        plugins={[dayGridPlugin]}
        initialView='dayGridMonth'
        weekends={true}
        events={events}
        eventContent={renderEventContent}
      />
    </div>
  )
}

// a custom render function
function renderEventContent(eventInfo) {
  return (
    <>
      <b>{eventInfo.timeText}</b>
      <i>{eventInfo.event.title}</i>
    </>
  )
}