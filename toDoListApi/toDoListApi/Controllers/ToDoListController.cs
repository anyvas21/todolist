using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using toDoListApi.Infrastructure;
using toDoListApi.Infrastructure.Services;
using toDoListApi.Model;

namespace toDoListApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    public class ToDoListController : ControllerBase
    {

        private readonly ILogger<ToDoListController> _logger;
        private ApplicationContext context = new ApplicationContext();

        public ToDoListController(ILogger<ToDoListController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "ItemList")]
        public async Task<ActionResult> ItemList()
        {
            ToDoListService toDoListService = new ToDoListService(context);
            var result = await toDoListService.GetAllItems();
            return (ActionResult)result;
        }

        [HttpGet(Name = "PieChartData")]
        public async Task<ActionResult> PieChartData()
        {
            ToDoListService toDoListService = new ToDoListService(context);
            var result = await toDoListService.ChartInfo();
            return (ActionResult)result;
        }

        [HttpGet(Name = "CalendarData")]
        public async Task<ActionResult> CalendarData()
        {
            ToDoListService toDoListService = new ToDoListService(context);
            var result = await toDoListService.CalendarInfo();
            return (ActionResult)result;
        }

        [HttpPost(Name = "CreateItem")]
        public async Task<ActionResult> CreateItem(ItemList item)
        {
            ToDoListService toDoListService = new ToDoListService(context);
            var result = await toDoListService.CreateItem(item);
            return (ActionResult)result;
        }

        [HttpPut(Name = "UpdateImportance")]
        public async Task<ActionResult> UpdateImportance([FromBody] int id)
        {
            ToDoListService toDoListService = new ToDoListService(context);
            var result = await toDoListService.ChangeImportance(id);
            return (ActionResult)result;
        }

        [HttpPut(Name = "UpdateDone")]
        public async Task<ActionResult> UpdateDone([FromBody] int id)
        {
            ToDoListService toDoListService = new ToDoListService(context);
            var result = await toDoListService.ChangeDone(id);
            return (ActionResult)result;
        }

        [HttpDelete(Name = "Delete")]
        public async Task<ActionResult> Delete([FromQuery] int id)
        {
            ToDoListService toDoListService = new ToDoListService(context);
            var result = await toDoListService.DeleteItem(id);
            return (ActionResult)result;
        }
    }
}