﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata;
using toDoListApi.Infrastructure;
using toDoListApi.Model;

namespace toDoListApi.Infrastructure.Services
{
    public class ToDoListService
    {
        public ApplicationContext _context { get; set; }

        public ToDoListService(ApplicationContext context) { _context = context; }

        public async Task<IActionResult> GetAllItems()
        {
            return new OkObjectResult(await GetAllItemsData());
        }

        public async Task<IActionResult> CreateItem(ItemList item)
        {
            _context.ItemList.Add(item);

            var created = await _context.SaveChangesAsync();

            if (created > 0)
            {
                return new OkObjectResult(await GetAllItemsData());
            }
            else
            {
                return new BadRequestResult();

            }
        }

        public async Task<IActionResult> DeleteItem(int id)
        {
            var item = _context.ItemList.Where(i => i.Id == id).FirstOrDefault();

            if (item == null)
            {
                return new BadRequestObjectResult("item not found");
            }

            _context.Remove(item);
            var created = _context.SaveChanges();

            if (created > 0)
            {
                return new OkObjectResult(await GetAllItemsData());
            }
            else
            {
                return new BadRequestResult();

            }
        }

        public async Task<IActionResult> ChangeImportance(int id)
        {
            var item = _context.ItemList.Where(i => i.Id == id).FirstOrDefault();

            if (item == null)
            {
                return new BadRequestObjectResult("item not found");
            }

            item.IsImportant = !item.IsImportant;

            var created = await _context.SaveChangesAsync();
            if (created > 0)
            {
                return new OkObjectResult(await GetAllItemsData());
            }
            else
            {
                return new BadRequestResult();

            }
        }

        public async Task<IActionResult> ChangeDone(int id)
        {
            var item = _context.ItemList.Where(i => i.Id == id).FirstOrDefault();

            if (item == null)
            {
                return new BadRequestObjectResult("item not found");
            }

            item.IsDone = !item.IsDone;

            var created = await _context.SaveChangesAsync();
            if (created > 0)
            {
                return new OkObjectResult(await GetAllItemsData());
            }
            else
            {
                return new BadRequestResult();
            }
        }

        public async Task<IActionResult> ChartInfo()
        {
            List<ChartData> listChart = new List<ChartData> { };

            var data = await GetAllItemsData();
            ChartData all = new ChartData()
            {
                Value = data.Count,
                Name = "All tasks",
                Colore = "#0088FE",
            };

            ChartData important = new ChartData()
            {
                Value = data.Where(d => d.IsImportant == true).Count(),
                Name = "Important tasks",
                Colore = "#00C49F",
            };

            ChartData done = new ChartData()
            {
                Value = data.Where(d => d.IsDone == true).Count(),
                Name = "Done tasks",
                Colore = "#FFBB28",
            };

            listChart.Add(all);
            listChart.Add(important);
            listChart.Add(done);

            return new OkObjectResult(listChart);
        }

        public async Task<IActionResult> CalendarInfo()
        {
            List<CalendarData> listCalendar = new List<CalendarData> { };

            var data = await GetAllItemsData();

            foreach (var item in data) {
                listCalendar.Add(new CalendarData
                {
                    IsDone = item.IsDone,
                    IsImportant = item.IsImportant,
                    Start = item.Date,
                    Title = item.Name
                });
            }

            return new OkObjectResult(listCalendar);
        }

        private async Task<List<ItemList>> GetAllItemsData()
        {
            var items = await _context.ItemList.OrderByDescending(i => i.Date).ToListAsync();

            return items;
        }
    }
}
