﻿using Microsoft.EntityFrameworkCore;
using toDoListApi.Model;

namespace toDoListApi.Infrastructure
{
    public class ApplicationContext : DbContext
    {
        public DbSet<ItemList> ItemList { get; set; }
        public ApplicationContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=db;Port=5432;Database=tasks;Username=postgres;Password=postgres");
        }
    }
}