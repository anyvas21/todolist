﻿namespace toDoListApi.Model
{
    public class ChartData
    {
        public int Value { get; set; }
        public string? Name { get; set; }
        public string Colore { get; set; }
    }
}
