﻿namespace toDoListApi.Model
{
    public class CalendarData
    {
        public string? Title { get; set; }
        public DateTime? Start { get; set; }
        public bool IsImportant { get; set; }
        public bool IsDone { get; set; }
    }
}
