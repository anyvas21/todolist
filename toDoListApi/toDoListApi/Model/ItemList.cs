﻿namespace toDoListApi.Model
{
    public class ItemList
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime? Date { get; set; }
        public bool IsImportant { get; set; }
        public bool IsDone { get; set; }
    }
}
